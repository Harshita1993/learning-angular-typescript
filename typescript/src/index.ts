//Basic types
let id: number = 5
let company: string = 'Learning TypeScript'
let isPublished: boolean = true
let x: any = 'Hello'

let ids: number[] = [1,2,3,4,5]
let arr: any[] = [1, 'hello', true]

//Tuple - specify exact types in the array
let person: [number, string, boolean] = [1, 'Harry', true]

//Tuple Array
let employee: [number, string][] 

employee = [
  [1, 'Jane'],
  [2, 'Harry'],
  [3, 'John']
]

//Union - to enable a variable to hold more than one type
let pid: string | number 
pid = 22 

//Enums - allow us to define a set of named constants either numeric or string
enum Direction1 {
  Up = 1,
  Down = 3,
  Left,
  Right
}
//by default each element is going to have values (0,1,2,3 etc)
// console.log(Direction1.Up) //1
// console.log(Direction1.Down) //3
// console.log(Direction1.Left) //4

enum Direction2 {
  Up = 'Up',
  Down = 'Down',
  Left = 'Left',
  Right = 'Right'
}
// console.log(Direction2.Up) //Up
// console.log(Direction2.Down) //Down
// console.log(Direction2.Left) //Left

//Objects
const User = {
  id: 1,
  name: 'John'
}
 //OR
type User = {
  id: number,
  name: string
}
const userDetails: User = {
  id: 2,
  name: 'Jane'
}

//Type Assertion: Explicitly telling the compiler that we want to treat an entity as a different type
let cid: any = 1
let customerId = <number>cid

//OR

let customersId = cid as number

//Functions
//It will ask for type (for x and y) unless you go to the tsconfig file and disable "noImplicitAny: true" to false
function addNum(x: number, y: number): number {
  return x + y
}
//console.log(addNum(1,2));

//Void
function log(message: string | number): void {
  console.log(message)
}

//Interfaces - Like a custom type or a specific structure to an object or a function
interface UserInterface {
  readonly id: number //read-only properties - can't be modified
  name: string
  age?: number //optional element
}

const user1: UserInterface = {
  id: 1,
  name: 'John'
}

//user1.id = 5 //will not work as it is readonly


//type can be used with primitives and union But interface CANNOT be used with primtives and union
type Point = number | string
const p1: Point = 1

//Function interface
interface MathFunc {
  (x: number, y: number): number
}

const add: MathFunc = (x: number, y: number): number => x + y
const sub: MathFunc = (x: number, y: number): number => x - y

//Classes
class Person {
  id: number //can be private or protected
  name: string

  //run whenever an object is instantiated
  constructor(id: number, name:string) {
    this.id = id,
    this.name = name
  }

  register(){
    return `${this.name} is now registered`
  }
}

//as soon as this is instantiated the constructor is executed
const brad = new Person(1, 'Brad Traversy')
const mike = new Person(2, 'Mike Jordan')

console.log(brad, mike);
console.log(mike.register())

//Implement interface in class
interface PersonInterface {
  id: number
  name: string
  register(): string
}

class PersonDetails implements PersonInterface {
  id: number
  name: string

  constructor(id: number, name:string) {
    this.id = id,
    this.name = name
  }

  register(){
    return `${this.name} is now registered`
  }
}

//Extending classes - inheritance, Subclasses
class Employee extends PersonDetails {
  position: string

  constructor(id: number, name: string, position: string) {
    super(id, name)
    this.position = position
  }
}

const emp = new Employee(12, 'Shane', 'Developer')
//console.log(emp.name)
//console.log(emp.register())

//Generics - Is used to build re-usable components
function getArray<T>(items: T[]): T[] {
  return new Array().concat(items)
}

let numArray = getArray<number>([1,2,3,4])
let strArray = getArray<string>(['Brad', 'John', 'Joe'])

//To not do this we used generic T
//numArray.push('Hello')