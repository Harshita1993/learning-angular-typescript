import { Component } from '@angular/core';

@Component({
  selector: 'app-root', //the info angular needs to replace with the template of the app component
  templateUrl: './app.component.html',
  //styleUrls: ['./app.component.css']
  styles:[`
    h3 {
      color: dodgerblue;
    }
  `]
})
export class AppComponent {
}
