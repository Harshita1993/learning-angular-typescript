//A blueprint for objects we create, what a recipe should look like
export class Recipe {
  public name: string
  public description: string
  public imagePath: string

  constructor(name: string, desc: string, imagePath: string) {
    this.name = name
    this.description = desc
    this.imagePath = imagePath
  }
}