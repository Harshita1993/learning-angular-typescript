import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  username = ''
  constructor() { }

  ngOnInit(): void {
  }

  onEnteredUsername(event) {
    this.username = (<HTMLInputElement>event.target).value
  }

  resetEnteredUsername() {
    this.username = ''
  }
}
