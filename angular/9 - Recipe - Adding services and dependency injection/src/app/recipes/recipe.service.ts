import { EventEmitter, Injectable } from "@angular/core";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Recipe } from "./recipe.model";

//Manage recipes here
@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>()

  private recipes: Recipe[] = [
    new Recipe(
      'Tasty Schnitzel', 
      'A super-tasty Schnitzel - just awesome!', 
      'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Breitenlesau_Krug_Br%C3%A4u_Schnitzel.JPG/1024px-Breitenlesau_Krug_Br%C3%A4u_Schnitzel.JPG',
      [
        new Ingredient('Chicken', 1),
        new Ingredient('French Fries', 20)
      ]),
    new Recipe(
      'Big fat burger', 
      'What else you need to say?', 
      'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Impossible_Burger_-_Gott%27s_Roadside-_2018_-_Stierch.jpg/1024px-Impossible_Burger_-_Gott%27s_Roadside-_2018_-_Stierch.jpg', 
      [
        new Ingredient('Buns', 2),
        new Ingredient('Chicken', 1)
      ])
  ];

  constructor(private slService: ShoppingListService){}

  getRecipes() {
    return this.recipes.slice()
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]){
    this.slService.addIngredients(ingredients)
  }
}